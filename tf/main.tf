terraform {
  required_version = ">= 1.1.0"
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = ">= 2.9.6"
    }
  }
}


provider "proxmox" {
  pm_api_url = "https://${var.pm_host}:8006/api2/json"
  pm_api_token_id = var.pm_token_id
  pm_api_token_secret = var.pm_token_secret
  pm_tls_insecure = true
}


resource "proxmox_vm_qemu" "pm_vm" {
  count = 3
  target_node = "pve"
  pool = "pm-vm-cluster-1"

  vmid = 1000 + count.index
  name = "vm-${count.index}"

  clone = "vm-template"
  os_type = "cloud-init"

  agent = 1
  sockets = 1
  cores = 2
  cpu = "host"

  memory = 1536
  balloon = 512

  disk {
    size = "32G"
    storage = "vmfs"
    type = "virtio"
  }

  network {
    bridge = "vmbr0"
    model = "virtio"
  }

  ipconfig0 = "ip=192.168.1.${count.index + 10}/24,gw=192.168.1.1"
}