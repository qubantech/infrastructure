# Infrastructure

## Creating virtual machines via Proxmox VE

Download cloud image to Proxmox node.

```
root@pve:~$ wget -nc https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img
```

Add `qemu-guest-agent` to `userconfig.yaml`.

```
root@pve:~$ nano /mnt/data/tffs/snippets/userconfig.yaml

#cloud-config
package_upgrade: false

runcmd:
  - apt-get install qemu-guest-agent -y
  - systemctl start qemu-guest-agent
```

Create template for virtual machine.

```
root@pve:~$ qm create 9000 \
	--name "vm-template" \
	--memory 4096 \
	--net0 virtio,bridge=vmbr1 \
	--scsihw virtio-scsi-pci \
	--scsi0 tffs:9000/vm-9000-disk-0.raw \
	--ide2 tffs:cloudinit \
	--boot c \
	--bootdisk scsi0 \
	--serial0 socket \
	--vga serial0 \
	--agent 1 \
	--ipconfig0 ip=192.168.1.0/24,gw=192.168.1.1 \
	--sshkey pub_key.pub \
	--cicustom "vendor=tffs:snippets/userconfig.yaml"
root@pve:~$ qm importdisk 9000 focal-server-cloudimg-amd64.img tffs
root@pve:~$ qm template 9000
```

## Declarative VM deployment with Terraform

```
ubuntu@server:~/tf$ nano main.tf

terraform {
  required_version = ">= 1.1.0"
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = ">= 2.9.6"
    }
  }
}


provider "proxmox" {
  pm_api_url = "https://${var.pm_host}:8006/api2/json"
  pm_api_token_id = var.pm_token_id
  pm_api_token_secret = var.pm_token_secret
  pm_tls_insecure = true
}


resource "proxmox_vm_qemu" "pm_vm" {
  count = 3
  target_node = "pve"
  pool = "pm-vm-cluster-1"

  vmid = 1000 + count.index
  name = "vm-${count.index}"

  clone = "vm-template"
  os_type = "cloud-init"

  agent = 1
  sockets = 1
  cores = 4
  cpu = "host"

  memory = 4096
  balloon = 512

  disk {
    size = "32G"
    storage = "tffs"
    type = "virtio"
  }

  network {
    bridge = "vmbr0"
    model = "virtio"
  }

  ipconfig0 = "ip=192.168.1.${count.index + 100}/24,gw=192.168.1.1"
}
```

Setup variables

```
ubuntu@server:~/tf$ nano variables.tf

variable "pm_host" {}

variable "pm_token_id" {}

variable "pm_token_secret" {}
```

Setup values

```
ubuntu@server:~/tf$ nano terraform.tfvars

pm_host = "192.168.1.3"
pm_token_id = "tfuser@pam!tftoken"
pm_token_secret = "XXXXXXXX-XXXX-XXXX-XXXX-7cbdf209b913"
```

Init and apply terraform config

```
ubuntu@server:~/tf$ terraform init
ubuntu@server:~/tf$ terraform apply
```

# Deploy K8S nodes with Kubespray

Clone Kubespray repository

```
git clone https://github.com/kubernetes-sigs/kubespray.git
```

Install Kubespray

```
cd kubespray
sudo pip3 install -r requirements.txt
```

Copy ``inventory/sample`` as ``inventory/mycluster``

```
cp -rfp inventory/sample inventory/mycluster
```

Specify node addresses

```
declare -a IPS=(192.168.2.10 192.168.2.11 192.168.2.12)
CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}
```

Review and change parameters under ``inventory/mycluster/group_vars``

```
cat inventory/mycluster/group_vars/all/all.yml
cat inventory/mycluster/group_vars/k8s_cluster/k8s-cluster.yml
```

Edit nodes configuration in ``inventory/mycluster/hosts.yaml``

Deploy Kubespray with Ansible Playbook - run the playbook as root
```

ansible-playbook -i inventory/mycluster/hosts.yaml  --user=ubuntu --become --become-user=root cluster.yml \
	--private-key ~/.ssh/secretkey
```